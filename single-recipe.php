<?php
/*
This is the custom post type post template.
If you edit the post type name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom post type is called
register_post_type( 'bookmarks',
then your single template should be
single-bookmarks.php

*/
?>
<?php get_header(); ?>
	<section class="recipe">
		<div class="row">
			<img src="http://placehold.it/312x312" class="column large-4"/>
			<div class="large-8 column">
				<h1>RECIPE NAMES HERE</h1>
				<div class="row recipes-steps">
					<ul class="column large-7">
						<li><h4>INGREDIENTS</h4></li>
						<li>Prepared pizza sauce</li>
						<li>TABASCO® brand Buffalo Style Hot Sauce</li>
						<li>Sour cream</li>
						<li>Mayonnaise</li>
						<li>Bleu cheese crumbles</li>
						<li>Prepared pizza dough</li>
						<li>Mozzarella cheese, shredded</li>
						<li>Chicken breast, cooked sliced</li>
					</ul>
					<ul class="column large-1">
						<li><h4>WEIGHT</h4></li>
						<li>12oz.</li>
						<li>6oz.</li>
						<li>6oz.</li>
						<li>7oz.</li>
						<li>10oz.</li>
						<li>6oz.</li>
						<li>6oz.</li>
					</ul>
					<ul class="column large-3">
						<li><h4>MEASURE</h4></li>
						<li>1 cup</li>
						<li>1/4 cup</li>
						<li>5 cup</li>
						<li>1 cup</li>
						<li>1/4 cup</li>
						<li>5 cup</li>
					</ul>
				</div>
			</div>			
		</div>
		<div class="row">
				<div class="method">
					<h4>METHOD</h4>
					<ol>
						<li>Combine the pizza sauce and half of TABASCO® Buffalo Style Hot Sauce. Reserve.</li>
						<li>In a separate bowl, mix together remaining TABASCO® Buffalo Style Hot Sauce, sour cream, mayonnaise, and bleu cheese crumbles.</li>
						<li>Roll out pizza dough until 1/4-inch thick and lay over oiled pizza screen. Coat dough in a thin layer of prepared sauce, leaving 1/3-inch for crust all the way around.</li>
						<li>Generously layer shredded mozzarella over the top; then lay sliced chicken over pizza.</li>
						<li>Bake in 450°F pre-heated oven 8 minutes and remove pizza from oven. Spoon bleu cheese, TABASCO® Buffalo Style Hot Sauce and sour cream mixture generously over the top. Cut into 8 slices.</li>
					</ol>					
				</div>
		</div>
	</section>

<?php get_footer(); ?>
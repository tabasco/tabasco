<?php
/*
Template Name: Contact Page
*/
?>


<?php get_header(); ?>
	<div class="row">
		<section id="contact">
			<form class="column large-6 medium-6">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
		    	<?php get_template_part( 'partials/loop', 'page' ); ?>
		    					
		    <?php endwhile; else : ?>
		
		   		<?php get_template_part( 'partials/content', 'missing' ); ?>

		    <?php endif; ?>
			</form>
			<div id="map" class="column large-6 medium-6">
				<iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Toronto%2C%20ON%2C%20Canada&key=AIzaSyAkmnkr7FmDPTenBLSCQYpXDNJ-ANSzpNE"></iframe>
				<p></p>
				<p></p>
			</div>
		</section>
	</div>
<?php get_footer(); ?>
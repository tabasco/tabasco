<!doctype html>

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<title><?php wp_title(''); ?></title>

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!-- mobile meta -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>

		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/main.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/about.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/home.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/contact.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/media.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/solution.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/recipe.css" type="text/css" media="screen" />

		<!--Font Awesome-->
		
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<!-- Drop Google Analytics here -->
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?> >
	<div role="mobile-navigation" class="mobile-nav">
		<a class="lang-toggle" href="" title="french">Francais</a>
		<?php get_search_form(); ?>
		<div class="nav-secondary">
			<?php wp_nav_menu( array('menu' => 'nav-secondary' )); ?>
		</div>
		<div class="nav-primary">
			<?php wp_nav_menu( array('menu' => 'nav-primary' )); ?>
		</div>
	</div>	
	<div class="off-canvas-wrap">
		<div class="inner-wrap">
			<div id="container">
				<header role="header">
	                <div class="nav-secondary">
	                	<div class="row">
	                		<div class="large-7 clearfix right">
	                			<div class="container right">
			                        <?php wp_nav_menu( array('menu' => 'nav-secondary' )); ?>
			                        <a href="" title="french">Francais</a>
			                        <?php get_search_form(); ?>
			                    </div>
		                    </div>
	                    </div>
	                </div>
	                <nav role="navigation">
	                    <div class="row">
	                        <div class="large-2 column">
                                <a title="logo" href="/">
                                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/logo.png" alt="logo-image">
                                </a>
	                        </div>
	                        <div class="large-10 column right">
	                            <?php wp_nav_menu( array('menu' => 'nav-primary' )); ?>
	                            <i class="fa fa-bars btn-mobile-nav"></i>
	                        </div>
	                    </div>
	                </nav>
	                <div class="update">
	                     <div class="row">
                            <div class="large-8 column right">
                                <p>Join us Wednesday Dec 10 for in-house Tabasco 'Plus-Ones' demo + recipe session</p>
                            </div>
	                    </div>  
	                </div>
	        	</header>
<?php
/*
Template Name: About Page
*/
?>

<?php get_header(); ?>
			
	<section class="tout">
		<div class="row">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_field('about_intro'); ?>
			<?php endwhile; endif; ?>
		</div>
	</section>
	<section>
		<div class="row">
		<?php the_field('about_secondary'); ?>
		</div>
	</section>
	<section>
		<div class="row">
			<?php the_field('about_tertiary'); ?>
		</div>
	</section>

<?php get_footer(); ?>
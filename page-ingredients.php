<?php
/*
Template Name: Ingredients Division 
*/
?>

<?php get_header(); ?>
	<section class="tout">
		<div class="row">
			<h1 class="tc white">WELCOME TO THE SOLUTION CENTRE</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi mattis, ipsum vitae dictum hendrerit, dolor eros volutpat eros, eu finibus sem erat a erat. Mauris lorem neque, euismod a luctus in, posuere eu odio. Mauris libero nibh, vehicula non purus egestas, ornare semper velit. Nullam ante elit, venenatis eu lectus in, laoreet vestibulum sapien. Nullam at tellus nec enim ultricies lacinia. Fusce euismod nisi ac vulputate faucibus. Quisque quis posuere lectus.</p>
		</div>
	</section>

	<section class="ingredients-wrap">
		<div class="row">
			<h2 style="text-align:center;">LIQUID  FORMULATIONS</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dictum libero non eros iaculis tincidunt. Aliquam erat volutpat. Morbi aliquet justo et nulla commodo, in porta leo dignissim. Etiam vitae suscipit lacus, quis gravida erat. Nam nisi eros, cursus quis metus rhoncus, euismod luctus massa. </p>
		</div>
		<div class="row item">
			<div class="large-3 medium-3 column text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/bottle.gif"/>
			</div>
			<div class="large-9 medium-9 column">
				<h3></h3>
				
				<P>After fermentation, the peppers are drained to remove excess moisture. The remaining mash is blended with high-quality vinegar, stirred for up to 28 days and then screened to remove the pepper seeds and skins.</p>
				<p>Nowhere else in the world can you get the unique flavor for which TABASCO® is famous. This is the gold standard of the Pepper Sauce category and is a world recognized icon. TABASCO® brand Original Red Sauce has long been used as a “chef's secret,” where a small amount provides a magical flavor enhancement to many foods. When you want the product that created the pepper sauce category, specify the original red liquid pepper seasoning -- TABASCO® brand Original Red Sauce.
</p>
				<p>APPLICATIONS<br/>
Use in all the traditional ways you know and in sauce and dressing applications, vacuum tumble applications and the processing of any reformed products.</p>
			</div>
		</div>
		<div class="row item">
			<div class="large-9 medium-9 column">
				<h3></h3> 
				<p>ll pepper sauces are not created equal. In 1868, Edmund McIlhenny began making his original pepper sauce from a unique combination of Capsicum frutescens peppers, Avery Island salt, French white wine vinegar and an oak-barrel aging process for the pepper mash of up to three years. More than 140 years later, the McIlhenny family holds steadfast to the traditional process.</p>
				<P>After fermentation, the peppers are drained to remove excess moisture. The remaining mash is blended with high-quality vinegar, stirred for up to 28 days and then screened to remove the pepper seeds and skins.</p>
				<p>Nowhere else in the world can you get the unique flavor for which TABASCO® is famous. This is the gold standard of the Pepper Sauce category and is a world recognized icon. TABASCO® brand Original Red Sauce has long been used as a “chef's secret,” where a small amount provides a magical flavor enhancement to many foods. When you want the product that created the pepper sauce category, specify the original red liquid pepper seasoning -- TABASCO® brand Original Red Sauce.
</p>
				<p>APPLICATIONS<br/>
Use in all the traditional ways you know and in sauce and dressing applications, vacuum tumble applications and the processing of any reformed products.</p>
			</div>
			<div class="large-3 medium-3 column text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/bottle.gif"/>
			</div>
		</div>
	</section>

	<section class="ingredients-wrap moisture">
		<div class="row">
			<h2 style="text-align:center;">LIQUID  FORMULATIONS</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dictum libero non eros iaculis tincidunt. Aliquam erat volutpat. Morbi aliquet justo et nulla commodo, in porta leo dignissim. Etiam vitae suscipit lacus, quis gravida erat. Nam nisi eros, cursus quis metus rhoncus, euismod luctus massa. </p>
		</div>
		<div class="row item">
			<div class="large-3 medium-3 column text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/ingredients.png"/>
			</div>
			<div class="large-9 medium-9 column">
				<h3></h3>
				
				<P>After fermentation, the peppers are drained to remove excess moisture. The remaining mash is blended with high-quality vinegar, stirred for up to 28 days and then screened to remove the pepper seeds and skins.</p>
				<p>Nowhere else in the world can you get the unique flavor for which TABASCO® is famous. This is the gold standard of the Pepper Sauce category and is a world recognized icon. TABASCO® brand Original Red Sauce has long been used as a “chef's secret,” where a small amount provides a magical flavor enhancement to many foods. When you want the product that created the pepper sauce category, specify the original red liquid pepper seasoning -- TABASCO® brand Original Red Sauce.
</p>
				<p>APPLICATIONS<br/>
Use in all the traditional ways you know and in sauce and dressing applications, vacuum tumble applications and the processing of any reformed products.</p>
			</div>
		</div>
		<div class="row item">
			<div class="large-9 medium-9 column">
				<h3></h3> 
				<p>ll pepper sauces are not created equal. In 1868, Edmund McIlhenny began making his original pepper sauce from a unique combination of Capsicum frutescens peppers, Avery Island salt, French white wine vinegar and an oak-barrel aging process for the pepper mash of up to three years. More than 140 years later, the McIlhenny family holds steadfast to the traditional process.</p>
				<P>After fermentation, the peppers are drained to remove excess moisture. The remaining mash is blended with high-quality vinegar, stirred for up to 28 days and then screened to remove the pepper seeds and skins.</p>
				<p>Nowhere else in the world can you get the unique flavor for which TABASCO® is famous. This is the gold standard of the Pepper Sauce category and is a world recognized icon. TABASCO® brand Original Red Sauce has long been used as a “chef's secret,” where a small amount provides a magical flavor enhancement to many foods. When you want the product that created the pepper sauce category, specify the original red liquid pepper seasoning -- TABASCO® brand Original Red Sauce.
</p>
				<p>APPLICATIONS<br/>
Use in all the traditional ways you know and in sauce and dressing applications, vacuum tumble applications and the processing of any reformed products.</p>
			</div>
			<div class="large-3 medium-3 column text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/ingredients.png"/>
			</div>
		</div>
	</section>

	<section class="ingredients-wrap">
		<div class="row">
			<h2 style="text-align:center;">LIQUID  FORMULATIONS</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dictum libero non eros iaculis tincidunt. Aliquam erat volutpat. Morbi aliquet justo et nulla commodo, in porta leo dignissim. Etiam vitae suscipit lacus, quis gravida erat. Nam nisi eros, cursus quis metus rhoncus, euismod luctus massa. </p>
		</div>
		<div class="row item">
			<div class="large-3 medium-3 column text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/bottle.gif"/>
			</div>
			<div class="large-9 medium-9 column">
				<h3></h3>
				
				<P>After fermentation, the peppers are drained to remove excess moisture. The remaining mash is blended with high-quality vinegar, stirred for up to 28 days and then screened to remove the pepper seeds and skins.</p>
				<p>Nowhere else in the world can you get the unique flavor for which TABASCO® is famous. This is the gold standard of the Pepper Sauce category and is a world recognized icon. TABASCO® brand Original Red Sauce has long been used as a “chef's secret,” where a small amount provides a magical flavor enhancement to many foods. When you want the product that created the pepper sauce category, specify the original red liquid pepper seasoning -- TABASCO® brand Original Red Sauce.
</p>
				<p>APPLICATIONS<br/>
Use in all the traditional ways you know and in sauce and dressing applications, vacuum tumble applications and the processing of any reformed products.</p>
			</div>
		</div>
		<div class="row item">
			<div class="large-9 medium-9 column">
				<h3></h3> 
				<p>ll pepper sauces are not created equal. In 1868, Edmund McIlhenny began making his original pepper sauce from a unique combination of Capsicum frutescens peppers, Avery Island salt, French white wine vinegar and an oak-barrel aging process for the pepper mash of up to three years. More than 140 years later, the McIlhenny family holds steadfast to the traditional process.</p>
				<P>After fermentation, the peppers are drained to remove excess moisture. The remaining mash is blended with high-quality vinegar, stirred for up to 28 days and then screened to remove the pepper seeds and skins.</p>
				<p>Nowhere else in the world can you get the unique flavor for which TABASCO® is famous. This is the gold standard of the Pepper Sauce category and is a world recognized icon. TABASCO® brand Original Red Sauce has long been used as a “chef's secret,” where a small amount provides a magical flavor enhancement to many foods. When you want the product that created the pepper sauce category, specify the original red liquid pepper seasoning -- TABASCO® brand Original Red Sauce.
</p>
				<p>APPLICATIONS<br/>
Use in all the traditional ways you know and in sauce and dressing applications, vacuum tumble applications and the processing of any reformed products.</p>
			</div>
			<div class="large-3 medium-3 column text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/bottle.gif"/>
			</div>
		</div>
	</section>
<?php get_footer(); ?>
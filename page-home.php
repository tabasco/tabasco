<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>
	
	<style>
		#about{
			background: url("<?php the_field('about_image'); ?>");
		}
		#solutions{
			background: url("<?php the_field('solutions_center_image'); ?>");
		}
		#chef{
			background: url("<?php the_field('meet_the_chef_image'); ?>");
		}
		#diamond-club{
			background: url("<?php the_field('diamond_club_image'); ?>");
		}
	</style>

	<section id="video">
		
	</section>
	<section id="quote">
	    <div class="row">
			<p><blockquote><?php the_field('intro_quote'); ?></blockquote></p>
		</div>
	</section>
	<section id="about">
		<div class="row">
			<?php the_field('about_blurb'); ?>
		</div>
		<div class="img-overlay"></div>
	</section>
	<section id="ingredients">
		<div class="row">
			<?php the_field('ingredients_division'); ?>
		</div>
	</section>
	<section id="solutions">
		<div class="row white">
			<?php the_field('solutions_center'); ?>
		</div>
	</section>
	<section id="chef">
		<div class="row white">
			<div class="right large-7 column">
				<?php the_field('meet_the_chef'); ?>
			</div>
		</div>
	</section>
	<section id="stories">
		<div class="row">
			<?php the_field('client_story'); ?>
			<div class="wrap">
				<ul class="large-block-grid-3 medium-block-grid-2">
					<?php
 
						if( have_rows('client_videos') ):

						    while ( have_rows('client_videos') ) : the_row();?>

							<li data-reveal-id="videoModal">
								<div id="videoModal" class="reveal-modal large" data-reveal="" style="visibility: visible; display: none; top: 0px; opacity: 1;">
									<div class="flex-video widescreen vimeo" style="display: block;">
										<iframe width="1280" height="720" src="http://www.youtube-nocookie.com/embed/<?php the_sub_field('video_link'); ?>" frameborder="0" allowfullscreen="" ></iframe>
									</div>
									<a class="close-reveal-modal">&#215;</a>
								</div>
								<img src="<?php the_sub_field('thumbnail'); ?>"/>
							</li>

						    <?php endwhile;

						endif;
						 
					?>
				</ul>
			</div>
		</div>
	</section>
	<section id="diamond-club">
		<div class="row white">
			<?php the_field('diamond_club'); ?>
		</div>
		<div class="img-overlay"></div>
	</section>
<?php get_footer(); ?>
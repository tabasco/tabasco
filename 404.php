<?php get_header(); ?>
	<div id="error-page" class="row">
		<div class="large-9">
			<h1>Page Not Found</h1>
		    <p>We're very sorry, but the page you requested has not been found! It may have been moved or deleted.</p>
		    <p>I'm not blaming you, but have you checked your address bar? There might be a typo in the URL.</p>
		    <p>If there isn't, you could try searching my website for the content you were looking for:</p>
		    <?php get_search_form(); ?>
		    <p>Or maybe you were looking for one of my popular posts:</p>
		</div>
	</div>
<?php get_footer(); ?>
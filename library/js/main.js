$(document).ready(function() {

	$('.home').tubular({videoId: 'l-jQlz3mpFE'});

	var winHeight = $( window ).height();

	$("#video").css("height", winHeight);

    var $btnMobile = $(".btn-mobile-nav");

    $btnMobile.on("click", function(){
        $("body").toggleClass("nav-toggle");
    });

    // Adding Data Attributes to the video reveal 

    $("#stories ul li:nth-child(1n)").attr('data-reveal-id', 'reveal-one');
    $("#stories ul li:nth-child(1n) > div").attr('id', 'reveal-one');
    $("#stories ul li:nth-child(2n)").attr('data-reveal-id', 'reveal-two');
    $("#stories ul li:nth-child(2n) > div").attr('id', 'reveal-two');
    $("#stories ul li:nth-child(3n)").attr('data-reveal-id', 'reveal-3');
    $("#stories ul li:nth-child(3n) > div").attr('id', 'reveal-3');
    $("#stories ul li:nth-child(4n)").attr('data-reveal-id', 'reveal-4');
    $("#stories ul li:nth-child(4n) > div").attr('id', 'reveal-4');
    $("#stories ul li:nth-child(5n)").attr('data-reveal-id', 'reveal-5');
    $("#stories ul li:nth-child(5n) > div").attr('id', 'reveal-5');
    $("#stories ul li:nth-child(6n)").attr('data-reveal-id', 'reveal-6');
    $("#stories ul li:nth-child(6n) > div").attr('id', 'reveal-6');

});
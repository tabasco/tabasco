					<div class="social-wrap large-12 hide-for-small-only">
						<div class="row">
							<div class="large-4 column">
								<?php echo do_shortcode("[custom-facebook-feed]"); ?>
							</div>
							<div class="large-4 column">
								<?php dynamic_sidebar('sidebar-1'); ?>
							</div>
							<div class="large-4 column blog">
								<?php $args = array( 'numberposts' => 2 ); $lastposts = get_posts( $args ); foreach($lastposts as $post) : setup_postdata($post); ?> <div class="post"><h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> <?php the_excerpt(); ?> </div><?php endforeach; ?>
							</div>
						</div>
					</div>
					<footer class="footer" role="contentinfo">
					
						<div id="inner-footer" class="row clearfix">
						
							<div class="row">
								<div class="copy large-5 medium-5 column">
									<p>TABASCO® is a registered trademark for sauces and other goods and services; TABASCO, the TABASCO bottle 
design and label designs are the exclusive property of McIlhenny Company, Avery Island, Louisiana, USA 70513.</p>
								</div>
								<div class="social large-2 medium-2 column">
									<ul>
										<li><a href=""><i class="fa fa-twitter"></i></a></li>
										<li><a href=""><i class="fa fa-youtube"></i></a></li>
									</ul>
								</div>
								<div class="nav large-5 medium-5 column">
									<?php wp_nav_menu(nav-footer); ?>
								</div>
		    				</div>
			      	
						</div> <!-- end #inner-footer -->			
					</footer> <!-- end .footer -->
				</div> <!-- end #container -->
			</div> <!-- end .inner-wrap -->
		</div> <!-- end .off-canvas-wrap -->
						
				<!-- all js scripts are loaded in library/joints.php -->
				<?php wp_footer(); ?>
				<script src="<?php echo get_template_directory_uri(); ?>/library/js/tubular.js" type="text/javascript"></script>
				<script src="<?php echo get_template_directory_uri(); ?>/library/js/main.js" type="text/javascript"></script>
	</body>

</html> <!-- end page -->
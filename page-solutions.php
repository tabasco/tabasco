<?php
/*
Template Name: Solutions Page
*/
?>

<?php get_header(); ?>
		
	<section class="tout">
		<div class="row">
			<h1 class="tc white">WELCOME TO THE SOLUTION CENTRE</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi mattis, ipsum vitae dictum hendrerit, dolor eros volutpat eros, eu finibus sem erat a erat. Mauris lorem neque, euismod a luctus in, posuere eu odio. Mauris libero nibh, vehicula non purus egestas, ornare semper velit. Nullam ante elit, venenatis eu lectus in, laoreet vestibulum sapien. Nullam at tellus nec enim ultricies lacinia. Fusce euismod nisi ac vulputate faucibus. Quisque quis posuere lectus.</p>
				<p>Cras dapibus, erat ut sagittis elementum, dolor lectus scelerisque ligula, ac vestibulum dolor justo at quam. Aliquam auctor mauris finibus nisl vehicula, in venenatis mauris mollis. Sed varius venenatis arcu id dignissim. Aenean posuere eros eget ipsum sagittis placerat ac a ex. Sed luctus pretium tincidunt. Sed tempus sed nulla tristique convallis. Praesent viverra tristique orci, vitae bibendum arcu cursus a. Duis nec eros in felis finibus lobortis.</p>
		</div>
	</section>
	<div id="solutions-page" class="row">
		<section class="row f-b-house">
			<br/>
			<h2 style="text-align:center; margin-bottom:40px;">SHOW ME SOLUTIONS</h2>
			<br/>
			<br/>
			<ul class="large-6	medium-6 column">
				<li><h3>FRONT OF HOUSE</h3></li>
				<li class="row">
					<img src="http://placehold.it/170x95" class="large-6 column vid-thumb" data-reveal-id="aModal"/>
					<div class="large-6 right column">
						<h4>VIDEO TITLE HERE</h4>
						<p>Description goes here. Description goes here. Description goes here. Description goes here.</p>
					</div>
					<div id="aModal" class="reveal-modal large" data-reveal="" style="visibility: visible; display: none; top: 0px; opacity: 1;">
						<div class="flex-video widescreen vimeo" style="display: block;">
							<iframe width="1280" height="720" src="http://www.youtube-nocookie.com/embed/wnXCopXXblE?rel=0" frameborder="0" allowfullscreen="" ></iframe>
						</div>
						<a class="close-reveal-modal">&#215;</a>
					</div>
				</li>
				<li class="row">
					<img src="http://placehold.it/170x95" class="large-6 column vid-thumb" data-reveal-id="bModal"/>
					<div class="large-6 right column">
						<h4>VIDEO TITLE HERE</h4>
						<p>Description goes here. Description goes here. Description goes here. Description goes here.</p>
					</div>
					<div id="bModal" class="reveal-modal large" data-reveal="" style="visibility: visible; display: none; top: 0px; opacity: 1;">
						<div class="flex-video widescreen vimeo" style="display: block;">
							<iframe width="1280" height="720" src="//www.youtube.com/embed/9d8wWcJLnFI" frameborder="0" allowfullscreen></iframe>
						</div>
						<a class="close-reveal-modal">&#215;</a>
					</div>
				</li>
				<li class="row">
					<img src="http://placehold.it/170x95" class="large-6 column vid-thumb" data-reveal-id="cModal"/>
					<div class="large-6 right column">
						<h4>VIDEO TITLE HERE</h4>
						<p>Description goes here. Description goes here. Description goes here. Description goes here.</p>
					</div>
					<div id="cModal" class="reveal-modal large" data-reveal="" style="visibility: visible; display: none; top: 0px; opacity: 1;">
						<div class="flex-video widescreen vimeo" style="display: block;">
							<iframe width="1280" height="720" src="http://www.youtube-nocookie.com/embed/wnXCopXXblE?rel=0" frameborder="0" allowfullscreen="" ></iframe>
						</div>
						<a class="close-reveal-modal">&#215;</a>
					</div>
				</li>
				<li class="row">
					<img src="http://placehold.it/170x95" class="large-6 column vid-thumb" data-reveal-id="dModal"/>
					<div class="large-6 right column">
						<h4>VIDEO TITLE HERE</h4>
						<p>Description goes here. Description goes here. Description goes here. Description goes here.</p>
					</div>
					<div id="dModal" class="reveal-modal large" data-reveal="" style="visibility: visible; display: none; top: 0px; opacity: 1;">
						<div class="flex-video widescreen vimeo" style="display: block;">
							<iframe width="1280" height="720" src="http://www.youtube-nocookie.com/embed/wnXCopXXblE?rel=0" frameborder="0" allowfullscreen="" ></iframe>
						</div>
						<a class="close-reveal-modal">&#215;</a>
					</div>
				</li>

			</ul>
			<ul class="large-6 medium-6 column">
				<li><h3>BACK OF HOUSE</h3></li>
				<li class="row">
					<img src="http://placehold.it/170x95" class="large-6 column vid-thumb" data-reveal-id="eModal"/>
					<div class="large-6 right column">
						<h4>VIDEO TITLE HERE</h4>
						<p>Description goes here. Description goes here. Description goes here. Description goes here.</p>
					</div>
					<div id="eModal" class="reveal-modal large" data-reveal="" style="visibility: visible; display: none; top: 0px; opacity: 1;">
						<div class="flex-video widescreen vimeo" style="display: block;">
							<iframe width="1280" height="720" src="http://www.youtube-nocookie.com/embed/wnXCopXXblE?rel=0" frameborder="0" allowfullscreen="" ></iframe>
						</div>
						<a class="close-reveal-modal">&#215;</a>
					</div>
				</li>
				<li class="row">
					<img src="http://placehold.it/170x95" class="large-6 column vid-thumb" data-reveal-id="fModal"/>
					<div class="large-6 right column">
						<h4>VIDEO TITLE HERE</h4>
						<p>Description goes here. Description goes here. Description goes here. Description goes here.</p>
					</div>
					<div id="fModal" class="reveal-modal large" data-reveal="" style="visibility: visible; display: none; top: 0px; opacity: 1;">
						<div class="flex-video widescreen vimeo" style="display: block;">
							<iframe width="1280" height="720" src="http://www.youtube-nocookie.com/embed/wnXCopXXblE?rel=0" frameborder="0" allowfullscreen="" ></iframe>
						</div>
						<a class="close-reveal-modal">&#215;</a>
					</div>
				</li>
				<li class="row">
					<img src="http://placehold.it/170x95" class="large-6 column vid-thumb" data-reveal-id="gModal"/>
					<div class="large-6 right column">
						<h4>VIDEO TITLE HERE</h4>
						<p>Description goes here. Description goes here. Description goes here. Description goes here.</p>
					</div>
					<div id="gModal" class="reveal-modal large" data-reveal="" style="visibility: visible; display: none; top: 0px; opacity: 1;">
						<div class="flex-video widescreen vimeo" style="display: block;">
							<iframe width="1280" height="720" src="http://www.youtube-nocookie.com/embed/wnXCopXXblE?rel=0" frameborder="0" allowfullscreen="" ></iframe>
						</div>
						<a class="close-reveal-modal">&#215;</a>
					</div>
				</li>
				<li class="row">
					<img src="http://placehold.it/170x95" class="large-6 column vid-thumb" data-reveal-id="hModal"/>
					<div class="large-6 right column">
						<h4>VIDEO TITLE HERE</h4>
						<p>Description goes here. Description goes here. Description goes here. Description goes here.</p>
					</div>
					<div id="hModal" class="reveal-modal large" data-reveal="" style="visibility: visible; display: none; top: 0px; opacity: 1;">
						<div class="flex-video widescreen vimeo" style="display: block;">
							<iframe width="1280" height="720" src="http://www.youtube-nocookie.com/embed/wnXCopXXblE?rel=0" frameborder="0" allowfullscreen="" ></iframe>
						</div>
						<a class="close-reveal-modal">&#215;</a>
					</div>
				</li>
			</ul>
		</section>
		<section id="solutions-recipes">
			<div>
				<h2 class="tc">RECIPES DATABASE</h2>
				<div>
					<input type="text"/>
				</div>
			</div>
			<ul class="row">
				<li class="row">
					<a href="/?recipe=test-recipe"><img src="http://placehold.it/167x143" class="column large-3 medium-3"/></a>
					<div class="column large-9 medium-9">
						<p>Small batch forage farm-to-table Banksy, Helvetica plaid sustainable fap gentrify crucifix polaroid Kickstarter. Street art typewriter YOLO gentrify, blog dreamcatcher leggings swag. Kitsch fixie biodiesel, tofu Blue Bottle Austin bespoke skateboard cray. Put a bird on it American Apparel selvage, disrupt vegan lumbersexual umami trust fund Banksy PBR&B butcher Portland hoodie small batch Schlitz.</p>
					</div>
				</li>
				<li class="row">
					<a href="/?recipe=test-recipe"><img src="http://placehold.it/167x143" class="column large-3 medium-3"/></a>
					<div class="column large-9 medium-9">
						<p>Small batch forage farm-to-table Banksy, Helvetica plaid sustainable fap gentrify crucifix polaroid Kickstarter. Street art typewriter YOLO gentrify, blog dreamcatcher leggings swag. Kitsch fixie biodiesel, tofu Blue Bottle Austin bespoke skateboard cray. Put a bird on it American Apparel selvage, disrupt vegan lumbersexual umami trust fund Banksy PBR&B butcher Portland hoodie small batch Schlitz.</p>
					</div>
				</li>
				<li class="row">
					<a href="/?recipe=test-recipe"><img src="http://placehold.it/167x143" class="column large-3 medium-3"/></a>
					<div class="column large-9 medium-9">
						<p>Small batch forage farm-to-table Banksy, Helvetica plaid sustainable fap gentrify crucifix polaroid Kickstarter. Street art typewriter YOLO gentrify, blog dreamcatcher leggings swag. Kitsch fixie biodiesel, tofu Blue Bottle Austin bespoke skateboard cray. Put a bird on it American Apparel selvage, disrupt vegan lumbersexual umami trust fund Banksy PBR&B butcher Portland hoodie small batch Schlitz.</p>
					</div>
				</li>
				<li class="row">
					<a href="/?recipe=test-recipe"><img src="http://placehold.it/167x143" class="column large-3 medium-3"/></a>
					<div class="column large-9 medium-9">
						<p>Small batch forage farm-to-table Banksy, Helvetica plaid sustainable fap gentrify crucifix polaroid Kickstarter. Street art typewriter YOLO gentrify, blog dreamcatcher leggings swag. Kitsch fixie biodiesel, tofu Blue Bottle Austin bespoke skateboard cray. Put a bird on it American Apparel selvage, disrupt vegan lumbersexual umami trust fund Banksy PBR&B butcher Portland hoodie small batch Schlitz.</p>
					</div>
				</li>
			</ul>
			
		</section>	
		<section id="solutions-benefits">
			<h2 class="tc">FRONT OF HOUSE SERVICES</h2>
			<div class="column large-4">
				<img />
				<h4>MENU BOARDS</h4>
				<p>Yes, that is correct, we would be thrilled to provide you these amazing promotional vehicles to help you drive bold flavoured Tabasco enhanced features in your restaurant. Come on, tap into the Bold flavour explosion, and convey that unmistakable message of your commitment to quality when you let your customers know you proudly serve Tabasco Brand Pepper Sauce.</p>
			</div>
			<div class="column large-4">
				<img />
				<h4>CADDIES</h4>
				<p>The “Power of the TABASCO® brand Pepper Sauce Diamond” creates a positive halo effect for your entire operation and providing TABASCO® on your tables signifies your commitment to delivering quality to your patrons. The TABASCO® brand Pepper Sauce Family of Flavours® provides your patrons with comfort and familiarity, while allowing flavour personalization. The TABASCO® brand Pepper Sauce Caddies will keep your tabletops nicely organized, as well as increase operational efficiency!</p>
			</div>
			<div class="column large-4">
				<img />
				<h4>LOCAL, CUSTOMIZED PROMOTIONS</h4>
				<p>Let us help heat up your sales. Whether inspired, new flavours and features from back of house, or innovative exotic beverages at the bar, or the freedom for patrons to augment their flavours at the table, Tabasco Brand Pepper Sauce can help ease take some heat off your patrons need for innovation.</p>
			</div>
		</section>
	</div>

<?php get_footer(); ?>